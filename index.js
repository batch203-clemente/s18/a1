function addTwoNumbers(a,b){
    console.log("Displayed sum of " + a + " and " + b);
    console.log(a + b);
}
addTwoNumbers(5,15);

function subtractTwoNumbers(a,b){
    console.log("Displayed difference of " + a + " and " + b);
    console.log(a - b);
}
subtractTwoNumbers(20,5);

function multiplyTwoNumbers(a,b){
    console.log("The product of " + a + " and " + b + ":");
    let product = a * b;
    return product;
}
let product = multiplyTwoNumbers(50,10);
console.log(product);

function divideTwoNumbers(a,b){
    console.log("The quotient of " + a + " and " + b + ":");
    let quotient = a / b;
    return quotient;
}
let quotient = divideTwoNumbers(50,10);
console.log(quotient);

function totalAreaOfCircle(radius, pi){
    console.log("The result of getting the area of a circle with " + radius + " radius:");
    let circleArea = pi * Math.pow(radius,2);
    return circleArea;
}
let circleArea = totalAreaOfCircle(15,3.1416);
console.log(circleArea);

function getTotalAverage(a,b,c,d){
    console.log("The average of " + a + "," + b + "," + c + "," + d + ":");
    let averageVar = (a + b + c + d) / 4;
    return averageVar;
}
let averageVar = getTotalAverage(20,40,60,80);
console.log(averageVar);

function isScorePassed(yourScore, totalScore){
    console.log("Is " + yourScore + "/" + totalScore + " a passing score?");
    let percentScore = (yourScore / totalScore) * 100;
    let isPassed = percentScore >= 75;
    return isPassed;
}
let isPassingScore = isScorePassed(38,50);
console.log(isPassingScore);